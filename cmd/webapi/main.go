package main

import (
	"flag"
	"log"

	"web-api/internal/app/model"
	"web-api/internal/app/webapi"

	"github.com/BurntSushi/toml"
)

var (
	configPath string
)

func init() {
	flag.StringVar(&configPath, "config-path", "./../../configs/webapi.toml", "path to config file")
}

// @title Boloto.io API
// @version 1.0
// @description Бэкенд для платформы Boloto.io
// @termsOfService http://swagger.io/terms/
func main() {
	if err := run(); err != nil {
		log.Fatal(err)
	}
}

func run() error {
	flag.Parse()

	config := &model.Config{}
	if _, err := toml.DecodeFile(configPath, config); err != nil {
		return err
	}

	if err := webapi.Start(config); err != nil {
		return err
	}

	return nil
}
