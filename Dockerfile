FROM golang:alpine3.15

WORKDIR /app

COPY ./configs ./configs
COPY ./cmd ./cmd
COPY ./internal ./internal
COPY ./pkg ./pkg
COPY ./docs ./docs
COPY ./go.mod ./
COPY ./go.sum ./

RUN go build ./cmd/webapi

EXPOSE 8081

CMD ["./webapi", "-config-path","./configs/webapi.toml"]