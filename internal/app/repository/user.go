package repository

import (
	"database/sql"
	"sync"

	"web-api/internal/app/model"
)

type UserRepository struct {
	db *sql.DB
	mu *sync.RWMutex
}

func NewUserRepository(db *sql.DB) *UserRepository {
	return &UserRepository{
		db: db,
		mu: &sync.RWMutex{},
	}
}

func (repo *UserRepository) GetById(id int64) (*model.User, error) {
	user := model.User{}

	sql := `select *
			from users
			where id = $1`

	repo.mu.RLock()
	err := repo.db.
		QueryRow(sql, id).
		Scan(&user.Id,
			&user.Email,
			&user.PasswordHash,
			&user.RegistrationDate,
			&user.Name,
			&user.Username,
			&user.Phone,
			&user.EmailConfirmed,
			&user.PhoneConfirmed)
	repo.mu.RUnlock()

	if err != nil {
		return nil, err
	}

	return &user, nil
}

func (repo *UserRepository) FindByEmail(email string) (*model.User, error) {
	user := model.User{}

	sql := `select *
			from users
			where email = $1`

	repo.mu.RLock()
	err := repo.db.
		QueryRow(sql, email).
		Scan(&user.Id,
			&user.Email,
			&user.PasswordHash,
			&user.RegistrationDate,
			&user.Name,
			&user.Username,
			&user.Phone,
			&user.EmailConfirmed,
			&user.PhoneConfirmed)
	repo.mu.RUnlock()

	if err != nil {
		return nil, err
	}

	return &user, nil
}

func (repo *UserRepository) Create(user *model.User) error {
	sql := `insert into users (email, password_hash, registration_date, name, username, phone, email_confirmed, phone_confirmed)
			values ($1, $2, $3, $4, $5, $6, $7, $8)
			returning id`

	repo.mu.Lock()
	err := repo.db.
		QueryRow(sql,
			user.Email,
			user.PasswordHash,
			user.RegistrationDate,
			user.Name,
			user.Username,
			user.Phone,
			user.EmailConfirmed,
			user.PhoneConfirmed).
		Scan(&user.Id)
	repo.mu.Unlock()

	if err != nil {
		return err
	}

	return nil
}

func (repo *UserRepository) LastId() (int64, error) {
	id := int64(0)

	sql := `select id
			from users
			order by id desc
			limit 1`

	repo.mu.RLock()
	err := repo.db.
		QueryRow(sql).
		Scan(&id)
	repo.mu.RUnlock()

	if err != nil {
		return 0, err
	}

	return id, nil
}
