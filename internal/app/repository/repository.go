package repository

import (
	"database/sql"

	"web-api/internal/app/model"
)

type Story interface {
	GetById(id int64) (*model.Story, error)
	GetAll(limit int) ([]*model.Story, error)
	GetByUserId(userId int64) ([]*model.Story, error)
	Create(user *model.Story) error
	UpdateViews(id, views int64) error
	UpdateLikes(id, likes int64) error
	GetLike(storyId, userId int64) (*model.Like, error)
	GetLikes(stories []*model.Story, userId int64) ([]*model.Like, error)
	CreateLike(like *model.Like) error
	DeleteLike(storyId, userId int64) error
}

type User interface {
	GetById(id int64) (*model.User, error)
	FindByEmail(email string) (*model.User, error)
	Create(user *model.User) error
	LastId() (int64, error)
}

type Tag interface {
	Search(query string, limit int) ([]*model.Tag, error)
}

type Repository struct {
	Story
	User
	Tag
}

func NewRepository(db *sql.DB) *Repository {
	return &Repository{
		Story: NewStoryRepository(db),
		User:  NewUserRepository(db),
		Tag:   NewTagRepository(db),
	}
}
