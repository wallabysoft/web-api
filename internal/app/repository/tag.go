package repository

import (
	"database/sql"
	"strings"
	"sync"

	"web-api/internal/app/model"
)

type TagRepository struct {
	db *sql.DB
	mu *sync.RWMutex
}

func NewTagRepository(db *sql.DB) *TagRepository {
	return &TagRepository{
		db: db,
		mu: &sync.RWMutex{},
	}
}

func (repo *TagRepository) Search(query string, limit int) ([]*model.Tag, error) {
	var tags []*model.Tag

	sql := `select *
			from tags
			where lower(name)
			like $1 limit $2`

	repo.mu.RLock()
	rows, err := repo.db.Query(sql, "%"+strings.ToLower(query)+"%", limit)
	repo.mu.RUnlock()

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		tag := &model.Tag{}

		err := rows.Scan(&tag.Id, &tag.Name)
		if err != nil {
			return nil, err
		}

		tags = append(tags, tag)
	}

	return tags, nil
}
