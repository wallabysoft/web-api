package repository

import (
	"database/sql"
	"strconv"
	"strings"
	"sync"

	"web-api/internal/app/model"
)

type StoryRepository struct {
	db *sql.DB
	mu *sync.RWMutex
}

func NewStoryRepository(db *sql.DB) *StoryRepository {
	return &StoryRepository{
		db: db,
		mu: &sync.RWMutex{},
	}
}

func (repo *StoryRepository) GetById(id int64) (*model.Story, error) {
	story := model.Story{}

	sql := `select *
			from stories
			where id = $1`

	repo.mu.RLock()
	err := repo.db.
		QueryRow(sql, id).
		Scan(&story.Id, &story.Title, &story.Text, &story.UserId, &story.IsAnonymous, &story.IsHidden, &story.Views, &story.Likes, &story.CreatedDate)
	repo.mu.RUnlock()

	if err != nil {
		return nil, err
	}

	return &story, nil
}

func (repo *StoryRepository) GetAll(limit int) ([]*model.Story, error) {
	var stories []*model.Story

	sql := `select *
			from stories
			order by created_date
			limit $1`

	repo.mu.RLock()
	rows, err := repo.db.Query(sql, limit)
	repo.mu.RUnlock()

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		story := &model.Story{}

		err := rows.Scan(&story.Id, &story.Title, &story.Text, &story.UserId, &story.IsAnonymous, &story.IsHidden, &story.Views, &story.Likes, &story.CreatedDate)
		if err != nil {
			return nil, err
		}
		stories = append(stories, story)
	}

	return stories, nil
}

func (repo *StoryRepository) GetByUserId(userId int64) ([]*model.Story, error) {
	stories := []*model.Story{}

	sql := `select *
			from stories
			where user_id = $1`

	repo.mu.RLock()
	rows, err := repo.db.Query(sql, userId)
	repo.mu.RUnlock()

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		story := &model.Story{}

		err := rows.Scan(&story.Id, &story.Title, &story.Text, &story.UserId, &story.IsAnonymous, &story.IsHidden, &story.Views, &story.Likes, &story.CreatedDate)
		if err != nil {
			return nil, err
		}

		stories = append(stories, story)
	}

	return stories, nil
}

func (repo *StoryRepository) Create(story *model.Story) error {
	sql := `insert into stories (title, text, user_id, is_anonymous, is_hidden, views, likes, created_date)
			values ($1, $2, $3, $4, $5, $6, $7, $8)
			returning id`

	repo.mu.Lock()
	err := repo.db.
		QueryRow(sql,
			story.Title,
			story.Text,
			story.UserId,
			story.IsAnonymous,
			story.IsHidden,
			story.Views,
			story.Likes,
			story.CreatedDate).
		Scan(&story.Id)
	repo.mu.Unlock()

	if err != nil {
		return err
	}

	return nil
}

func (repo *StoryRepository) UpdateViews(id, views int64) error {
	sql := `update stories
			set views = $1
			where id = $2`

	_, err := repo.db.Exec(sql, views, id)
	if err != nil {
		return err
	}

	return nil
}

func (repo *StoryRepository) UpdateLikes(id, likes int64) error {
	sql := `update stories
			set likes = $1
			where id = $2`

	_, err := repo.db.Exec(sql, likes, id)
	if err != nil {
		return err
	}

	return nil
}

func (repo *StoryRepository) GetLike(storyId, userId int64) (*model.Like, error) {
	like := model.Like{}

	sql := `select *
			from likes
			where story_id = $1 and user_id = $2`

	repo.mu.RLock()
	err := repo.db.
		QueryRow(sql, storyId, userId).
		Scan(&like.StoryId, &like.UserId, &like.Date)
	repo.mu.RUnlock()

	if err != nil {
		return nil, err
	}

	return &like, nil
}

func (repo *StoryRepository) GetLikes(stories []*model.Story, userId int64) ([]*model.Like, error) {
	likes := []*model.Like{}

	if len(stories) == 0 {
		return likes, nil
	}

	values := []string{}
	for index := range stories {
		values = append(values, strconv.FormatInt(stories[index].Id, 10))
	}

	sql := `select *
			from likes
			where story_id in (` + strings.Join(values, ",") + `) and user_id = $1`

	repo.mu.RLock()
	rows, err := repo.db.Query(sql, userId)
	repo.mu.RUnlock()

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		like := &model.Like{}

		err := rows.Scan(&like.StoryId, &like.UserId, &like.Date)
		if err != nil {
			return nil, err
		}
		likes = append(likes, like)
	}

	return likes, nil
}

func (repo *StoryRepository) CreateLike(like *model.Like) error {
	sql := `insert into likes (story_id, user_id, date)
			values ($1, $2, $3)`

	_, err := repo.db.Exec(sql, like.StoryId, like.UserId, like.Date)
	if err != nil {
		return err
	}

	return nil
}

func (repo *StoryRepository) DeleteLike(storyId, userId int64) error {
	sql := `delete from likes
			where story_id = $1 and user_id = $2`

	_, err := repo.db.Exec(sql, storyId, userId)
	if err != nil {
		return err
	}

	return nil
}
