package model

import (
	"time"
)

type Like struct {
	StoryId int64     `json:"storyId"`
	UserId  int64     `json:"userId"`
	Date    time.Time `json:"date"`
}
