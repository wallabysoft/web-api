package model

import (
	"time"
)

type Story struct {
	Id          int64     `json:"id"`
	Title       string    `json:"title"`
	Text        string    `json:"text"`
	UserId      int64     `json:"userId"`
	IsAnonymous bool      `json:"isAnonymous"`
	IsHidden    bool      `json:"isHidden"`
	Views       int64     `json:"views"`
	Likes       int64     `json:"likes"`
	CreatedDate time.Time `json:"createdDate"`
}
