package model

type LoginRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type RegistrationRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type AddStoryRequest struct {
	Title       string `json:"title"`
	Text        string `json:"text"`
	IsAnonymous bool   `json:"isAnonymous"`
}
