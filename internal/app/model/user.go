package model

import (
	"database/sql"
	"time"
)

type User struct {
	Id               int64
	Email            string
	PasswordHash     string
	RegistrationDate time.Time
	Name             string
	Username         string
	Phone            sql.NullString
	EmailConfirmed   bool
	PhoneConfirmed   bool
}
