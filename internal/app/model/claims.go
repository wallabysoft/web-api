package model

import "github.com/dgrijalva/jwt-go"

type Claims struct {
	UserId int64
	jwt.StandardClaims
}
