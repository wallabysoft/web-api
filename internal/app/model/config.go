package model

type Database struct {
	Host     string `toml:"host"`
	Port     int    `toml:"port"`
	User     string `toml:"user"`
	Password string `toml:"password"`
	DbName   string `toml:"dbname"`
	SslMode  string `toml:"sslmode"`
}

type JWT struct {
	Audience string `toml:"audience"`
	Expires  int    `toml:"expires"`
	Issuer   string `toml:"issuer"`
	Key      string `toml:"key"`
}

type Config struct {
	Addr     string   `toml:"addr"`
	Database Database `toml:"database"`
	JWT      JWT      `toml:"jwt"`
}
