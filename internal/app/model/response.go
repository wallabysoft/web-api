package model

import "time"

type Profile struct {
	Email            string    `json:"email"`
	Name             string    `json:"name"`
	Username         string    `json:"username"`
	RegistrationDate time.Time `json:"registrationDate"`
}

type AuthResponse struct {
	Token   string  `json:"token"`
	Profile Profile `json:"profile"`
}

type StoryResponse struct {
	Story
	IsLiked bool `json:"isLiked"`
}
