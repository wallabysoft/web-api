package webapi

import (
	"context"
	"net/http"
	"strings"

	"web-api/internal/app/handler"
	"web-api/internal/app/model"

	_ "web-api/docs"

	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"github.com/sirupsen/logrus"
	"github.com/swaggo/http-swagger"
)

type Server struct {
	config *model.Config
	http   *http.Server
	logger *logrus.Logger
}

const swaggerPath = "/swagger"

var allowedPaths = []string{
	"/account/login",
	"/account/registration",
	"/stories/random",
}

func newServer(config *model.Config, logger *logrus.Logger) *Server {
	return &Server{
		config: config,
		logger: logger,
	}
}

func (s *Server) Run(handler *handler.Handler) error {
	s.http = &http.Server{
		Addr:    s.config.Addr,
		Handler: s.configureRouter(handler),
	}

	return s.http.ListenAndServe()
}

func (s *Server) Shutdown(ctx context.Context) error {
	return s.http.Shutdown(ctx)
}

func (s *Server) configureRouter(handler *handler.Handler) http.Handler {
	router := mux.NewRouter()

	router.Use(s.logRequest)
	router.Use(s.cors)
	router.Use(s.swagger)
	router.Use(s.authentication)

	router.HandleFunc("/account/login", handler.Account.Login).Methods(http.MethodPost)
	router.HandleFunc("/account/registration", handler.Account.Registration).Methods(http.MethodPost)
	router.HandleFunc("/account/profile", handler.Account.Profile).Methods(http.MethodGet)

	router.HandleFunc("/stories/profile", handler.Stories.Profile).Methods(http.MethodGet)
	router.HandleFunc("/stories/recommendations", handler.Stories.Recommendations).Methods(http.MethodGet)
	router.HandleFunc("/stories/random", handler.Stories.Random).Methods(http.MethodGet)
	router.HandleFunc("/stories/add", handler.Stories.Add).Methods(http.MethodPost)
	router.HandleFunc("/stories/like/{id:[0-9]+}", handler.Stories.Like).Methods(http.MethodGet)

	router.HandleFunc("/tags/search", handler.Tags.Search).Queries("q", "{q}").Methods(http.MethodGet)

	router.PathPrefix(swaggerPath).Handler(httpSwagger.Handler(
		httpSwagger.URL("/swagger/doc.json"),
	))

	return router
}

func (s *Server) logRequest(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		s.logger.WithFields(logrus.Fields{
			"method":      r.Method,
			"remote_addr": r.RemoteAddr,
			"url":         r.URL.Path,
		}).Info("New request")
		next.ServeHTTP(w, r)
	})
}

func (s *Server) cors(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_handler := cors.New(cors.Options{
			AllowedOrigins: []string{"*"},
			AllowedMethods: []string{"POST", "GET", "OPTIONS", "PUT", "DELETE"},
			AllowedHeaders: []string{"Accept", "Accept-Language", "Content-Type"},
		}).Handler(next)
		_handler.ServeHTTP(w, r)
	})
}

// Проверка доступа к swagger'у на продакшене
func (s *Server) swagger(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.Contains(r.URL.Path, swaggerPath) && isProduction(r.Host) {
			unauthorizedRequest(w)
			return
		}
		next.ServeHTTP(w, r)
	})
}

func (s *Server) authentication(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if !strings.Contains(r.URL.Path, swaggerPath) && !contains(allowedPaths, r.URL.Path) {
			var authenticationHeader = r.Header.Get("Authentication")
			if len(authenticationHeader) == 0 {
				unauthorizedRequest(w)
				return
			}

			var tokenParts = strings.Split(authenticationHeader, " ")
			if len(tokenParts) != 2 {
				unauthorizedRequest(w)
				return
			}

			var claims model.Claims

			token, err := jwt.ParseWithClaims(tokenParts[1], &claims, func(token *jwt.Token) (interface{}, error) {
				return []byte(s.config.JWT.Key), nil
			})

			if !token.Valid || err != nil {
				unauthorizedRequest(w)
				return
			}

			var ctx = context.WithValue(r.Context(), "UserId", claims.UserId)
			r = r.WithContext(ctx)
		}

		next.ServeHTTP(w, r)
	})
}

func isProduction(host string) bool {
	// TODO: boloto.io из конфига
	return strings.Compare(host, "boloto.io") == 0
}

func contains(items []string, search string) bool {
	for _, item := range items {
		if strings.Compare(item, search) == 0 {
			return true
		}
	}
	return false
}

func unauthorizedRequest(w http.ResponseWriter) {
	w.WriteHeader(http.StatusUnauthorized)
	_, err := w.Write([]byte("Доступ запрещён"))
	if err != nil {
		return
	}
}
