package webapi

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"web-api/internal/app/handler"
	"web-api/internal/app/model"
	"web-api/internal/app/repository"
	"web-api/pkg/util"

	"github.com/sirupsen/logrus"
)

func Start(config *model.Config) error {
	log := logrus.New()
	log.Out = os.Stdout
	log.SetFormatter(new(util.MyLogFormatter))

	db, err := newDB(config)
	if err != nil {
		return err
	}

	repo := repository.NewRepository(db)
	handler := handler.NewHandler(config, log, repo)

	srv := newServer(config, log)

	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		if err := srv.Run(handler); err != nil && err != http.ErrServerClosed {
			log.Fatal(err)
		}
	}()

	log.Info("Server started...")
	<-done
	log.Info("Server stopped...")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer func() {
		if err := db.Close(); err != nil {
			log.Fatal(err)
		}
		cancel()
	}()

	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server stopped unsuccessfully")
	}

	log.Info("Server stopped successfully")

	return nil
}
