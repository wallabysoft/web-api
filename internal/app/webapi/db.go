package webapi

import (
	"database/sql"
	"fmt"

	"web-api/internal/app/model"

	_ "github.com/lib/pq"
)

func newDB(config *model.Config) (*sql.DB, error) {
	db, err := sql.Open("postgres",
		fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=%s",
			config.Database.Host,
			config.Database.Port,
			config.Database.User,
			config.Database.Password,
			config.Database.DbName,
			config.Database.SslMode))

	if err != nil {
		return nil, err
	}

	if err := db.Ping(); err != nil {
		return nil, err
	}

	return db, nil
}
