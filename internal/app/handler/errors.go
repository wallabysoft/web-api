package handler

import "errors"

var (
	ErrInvalidForm                       = errors.New("Неверно заполнена форма")
	ErrIncorrectPassword                 = errors.New("Неверный пароль")
	ErrUserNotFound                      = errors.New("Пользователь не найден")
	ErrUserAlreadyExists                 = errors.New("Такой пользователь уже существует")
	ErrFailedToLogin                     = errors.New("Не удалось выполнить вход")
	ErrFailedToRegister                  = errors.New("Не удалось зарегистрироваться")
	ErrFailedToGetProfileStories         = errors.New("Не удалось получить истории профиля")
	ErrFailedToGetRecommendationsStories = errors.New("Не удалось получить рекомендуемые истории")
	ErrFailedToGetRandomStory            = errors.New("Не удалось получить случайную историю")
	ErrFailedToAddStory                  = errors.New("Не удалось добавить историю")
	ErrFailedToLikeStory                 = errors.New("Не удалось обработать лайк")
	ErrFailedToSearchTags                = errors.New("Не удалось найти теги")
	ErrFailedToProfile                   = errors.New("Не удалось получить информацию по профилю")
)
