package handler

import (
	"net/http"

	"web-api/internal/app/repository"
	"web-api/pkg/util"

	"github.com/sirupsen/logrus"
)

type TagsHandler struct {
	logger *logrus.Logger
	repo   repository.Tag
}

func NewTagsHandler(logger *logrus.Logger, repo repository.Tag) *TagsHandler {
	return &TagsHandler{
		logger: logger,
		repo:   repo,
	}
}

// Search godoc
// @Summary Поиск тегов
// @Description Поиск тегов по названию
// @Tags Tags
// @Produce json
// @Param query query string true "Query"
// @Param Authentication header string true "Authentication" default(Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySWQiOjIsImF1ZCI6IndlYi1hcHAiLCJleHAiOjE2NDQwMjA2ODAsImlzcyI6IndlYi1hcGkifQ.OhG88pH6ArKjKTrQ0aNL3Ffy3Z3a5-fvXqhzuZCrqFo)
// @Success 200 {array} model.Tag
// @Router /tags/search [get]
func (h *TagsHandler) Search(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query()

	tags, err := h.repo.Search(query["q"][0], 10)
	if err != nil {
		h.logger.Error(err.Error())
		util.BadRequest(w, ErrFailedToSearchTags.Error())
		return
	}

	util.JSONResponse(w, tags)
}
