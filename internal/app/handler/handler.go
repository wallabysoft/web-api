package handler

import (
	"net/http"

	"web-api/internal/app/model"
	"web-api/internal/app/repository"

	"github.com/sirupsen/logrus"
)

type Account interface {
	Login(w http.ResponseWriter, r *http.Request)
	Registration(w http.ResponseWriter, r *http.Request)
	Profile(w http.ResponseWriter, r *http.Request)
}

type Stories interface {
	Profile(w http.ResponseWriter, r *http.Request)
	Recommendations(w http.ResponseWriter, r *http.Request)
	Random(w http.ResponseWriter, r *http.Request)
	Add(w http.ResponseWriter, r *http.Request)
	Like(w http.ResponseWriter, r *http.Request)
}

type Tags interface {
	Search(w http.ResponseWriter, r *http.Request)
}

type Handler struct {
	Account
	Stories
	Tags
}

func NewHandler(config *model.Config, logger *logrus.Logger, repo *repository.Repository) *Handler {
	return &Handler{
		Account: NewAccountHandler(config, logger, repo.User),
		Stories: NewStoriesHandler(logger, repo.Story),
		Tags:    NewTagsHandler(logger, repo.Tag),
	}
}
