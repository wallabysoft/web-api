package handler

import (
	"encoding/json"
	"math/rand"
	"net/http"
	"strconv"
	"time"

	"web-api/internal/app/model"
	"web-api/internal/app/repository"
	"web-api/pkg/util"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
)

type StoriesHandler struct {
	logger *logrus.Logger
	repo   repository.Story
}

func NewStoriesHandler(logger *logrus.Logger, repo repository.Story) *StoriesHandler {
	return &StoriesHandler{
		logger: logger,
		repo:   repo,
	}
}

// TODO: вынести в отдельный пакет
func search(items []*model.Like, storyId int64) bool {
	for index := range items {
		if items[index].StoryId == storyId {
			return true
		}
	}
	return false
}

// Profile godoc
// @Summary Истории профиля
// @Description Получить истории профиля
// @Tags Stories
// @Produce json
// @Param Authentication header string true "Authentication" default(Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySWQiOjIsImF1ZCI6IndlYi1hcHAiLCJleHAiOjE2NDQwMjA2ODAsImlzcyI6IndlYi1hcGkifQ.OhG88pH6ArKjKTrQ0aNL3Ffy3Z3a5-fvXqhzuZCrqFo)
// @Success 200 {array} model.StoryResponse
// @Router /stories/profile [get]
func (h *StoriesHandler) Profile(w http.ResponseWriter, r *http.Request) {
	userId, ok := r.Context().Value("UserId").(int64)
	if !ok {
		h.logger.Error(ErrFailedToGetProfileStories.Error())
		util.BadRequest(w, ErrFailedToGetProfileStories.Error())
		return
	}

	stories, err := h.repo.GetByUserId(userId)
	if err != nil {
		h.logger.Error(err.Error())
		util.BadRequest(w, ErrFailedToGetProfileStories.Error())
		return
	}

	// Обновление просмотров
	for index := range stories {
		views := stories[index].Views + 1
		if err := h.repo.UpdateViews(stories[index].Id, views); err != nil {
			h.logger.Error(err.Error())
			util.BadRequest(w, ErrFailedToGetProfileStories.Error())
			return
		}
	}

	likes, err := h.repo.GetLikes(stories, userId)
	if err != nil {
		h.logger.Error(err.Error())
		util.BadRequest(w, ErrFailedToGetProfileStories.Error())
		return
	}

	result := []model.StoryResponse{}
	for index := range stories {
		isLiked := search(likes, stories[index].Id)
		item := model.StoryResponse{
			Story:   *stories[index],
			IsLiked: isLiked,
		}
		result = append(result, item)
	}

	util.JSONResponse(w, result)
}

// Recommendations godoc
// @Summary Рекомендованные истории
// @Description Получить рекомендованные истории
// @Tags Stories
// @Produce json
// @Param Authentication header string true "Authentication" default(Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySWQiOjIsImF1ZCI6IndlYi1hcHAiLCJleHAiOjE2NDQwMjA2ODAsImlzcyI6IndlYi1hcGkifQ.OhG88pH6ArKjKTrQ0aNL3Ffy3Z3a5-fvXqhzuZCrqFo)
// @Success 200 {array} model.StoryResponse
// @Router /stories/recommendations [get]
func (h *StoriesHandler) Recommendations(w http.ResponseWriter, r *http.Request) {
	userId, ok := r.Context().Value("UserId").(int64)
	if !ok {
		h.logger.Error(ErrFailedToGetProfileStories.Error())
		util.BadRequest(w, ErrFailedToGetProfileStories.Error())
		return
	}

	stories, err := h.repo.GetAll(10)
	if err != nil {
		h.logger.Error(err.Error())
		util.BadRequest(w, ErrFailedToGetProfileStories.Error())
	}

	// Обновление просмотров
	for index := range stories {
		views := stories[index].Views + 1
		if err := h.repo.UpdateViews(stories[index].Id, views); err != nil {
			h.logger.Error(err.Error())
			util.BadRequest(w, ErrFailedToGetProfileStories.Error())
			return
		}
	}

	likes, err := h.repo.GetLikes(stories, userId)
	if err != nil {
		h.logger.Error(err.Error())
		util.BadRequest(w, ErrFailedToGetProfileStories.Error())
		return
	}

	result := []model.StoryResponse{}
	for index := range stories {
		isLiked := search(likes, stories[index].Id)
		item := model.StoryResponse{
			Story:   *stories[index],
			IsLiked: isLiked,
		}
		result = append(result, item)
	}

	util.JSONResponse(w, result)
}

// Random godoc
// @Summary Случайная история
// @Description Получить случайную историю
// @Tags Stories
// @Produce json
// @Success 200 {object} model.StoryResponse
// @Router /stories/random [get]
func (h *StoriesHandler) Random(w http.ResponseWriter, r *http.Request) {
	rand.Seed(time.Now().UnixNano())
	id := int64(rand.Intn(1001-1) + 1)

	story, err := h.repo.GetById(id)
	if err != nil {
		h.logger.Error(err.Error())
		util.BadRequest(w, ErrFailedToGetRandomStory.Error())
	}

	// Обновление просмотров
	views := story.Views + 1
	if err := h.repo.UpdateViews(story.Id, views); err != nil {
		h.logger.Error(err.Error())
		util.BadRequest(w, ErrFailedToGetRandomStory.Error())
		return
	}

	result := model.StoryResponse{
		Story:   *story,
		IsLiked: false,
	}

	util.JSONResponse(w, result)
}

// Add godoc
// @Summary Добавить историю
// @Description Добавить историю
// @Tags Stories
// @Accept json
// @Produce json
// @Param story body model.AddStoryRequest true "Story"
// @Param Authentication header string true "Authentication" default(Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySWQiOjIsImF1ZCI6IndlYi1hcHAiLCJleHAiOjE2NDQwMjA2ODAsImlzcyI6IndlYi1hcGkifQ.OhG88pH6ArKjKTrQ0aNL3Ffy3Z3a5-fvXqhzuZCrqFo)
// @Success 200 {object} model.StoryResponse
// @Router /stories/add [post]
func (h *StoriesHandler) Add(w http.ResponseWriter, r *http.Request) {
	userId, ok := r.Context().Value("UserId").(int64)
	if !ok {
		h.logger.Error(ErrFailedToAddStory.Error())
		util.BadRequest(w, ErrFailedToAddStory.Error())
		return
	}

	request := model.AddStoryRequest{}

	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		h.logger.Error(err.Error())
		util.BadRequest(w, ErrFailedToAddStory.Error())
		return
	}

	// TODO: валидация формы
	if len(request.Title) == 0 || len(request.Text) == 0 {
		util.BadRequest(w, ErrInvalidForm.Error())
		return
	}

	newStory := &model.Story{
		Title:       request.Title,
		Text:        request.Text,
		UserId:      userId,
		IsAnonymous: request.IsAnonymous,
		IsHidden:    false,
		Views:       0,
		Likes:       0,
		CreatedDate: time.Now(),
	}

	if err := h.repo.Create(newStory); err != nil {
		h.logger.Error(err.Error())
		util.BadRequest(w, ErrFailedToAddStory.Error())
		return
	}

	result := model.StoryResponse{
		Story:   *newStory,
		IsLiked: false,
	}

	util.JSONResponse(w, result)
}

// Like godoc
// @Summary Поставить/убрать лайк у истории
// @Description Поставить/убрать лайк у истории по идентификатору
// @Tags Stories
// @Param id path int true "Story id"
// @Param Authentication header string true "Authentication" default(Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySWQiOjIsImF1ZCI6IndlYi1hcHAiLCJleHAiOjE2NDQwMjA2ODAsImlzcyI6IndlYi1hcGkifQ.OhG88pH6ArKjKTrQ0aNL3Ffy3Z3a5-fvXqhzuZCrqFo)
// @Success 200 {string} Ok
// @Router /stories/like/{id} [get]
func (h *StoriesHandler) Like(w http.ResponseWriter, r *http.Request) {
	userId, ok := r.Context().Value("UserId").(int64)
	if !ok {
		h.logger.Error(ErrFailedToLikeStory.Error())
		util.BadRequest(w, ErrFailedToLikeStory.Error())
		return
	}

	vars := mux.Vars(r)

	storyId, err := strconv.ParseInt(vars["id"], 10, 64)
	if err != nil {
		h.logger.Error(err.Error())
		util.BadRequest(w, ErrFailedToLikeStory.Error())
		return
	}

	story, err := h.repo.GetById(storyId)
	if err != nil {
		h.logger.Error(err.Error())
		util.BadRequest(w, ErrFailedToLikeStory.Error())
	}

	like, _ := h.repo.GetLike(storyId, userId)
	if like != nil {
		likes := story.Likes - 1
		if err := h.repo.UpdateLikes(storyId, likes); err != nil {
			h.logger.Error(err.Error())
			util.BadRequest(w, ErrFailedToLikeStory.Error())
			return
		}
		if err := h.repo.DeleteLike(storyId, like.UserId); err != nil {
			h.logger.Error(err.Error())
			util.BadRequest(w, ErrFailedToLikeStory.Error())
			return
		}
	} else {
		likes := story.Likes + 1
		if err := h.repo.UpdateLikes(storyId, likes); err != nil {
			h.logger.Error(err.Error())
			util.BadRequest(w, ErrFailedToLikeStory.Error())
			return
		}
		newLike := &model.Like{
			StoryId: storyId,
			UserId:  userId,
			Date:    time.Now(),
		}
		if err := h.repo.CreateLike(newLike); err != nil {
			h.logger.Error(err.Error())
			util.BadRequest(w, ErrFailedToLikeStory.Error())
			return
		}
	}

	util.OkRequest(w, "Ok")
}
