package handler

import (
	"database/sql"
	"encoding/json"
	"net/http"
	"strconv"
	"time"

	"web-api/internal/app/model"
	"web-api/internal/app/repository"
	"web-api/pkg/util"

	"github.com/sirupsen/logrus"
)

type AccountHandler struct {
	config *model.Config
	logger *logrus.Logger
	repo   repository.User
}

func NewAccountHandler(config *model.Config, logger *logrus.Logger, repo repository.User) *AccountHandler {
	return &AccountHandler{
		config: config,
		logger: logger,
		repo:   repo,
	}
}

// Login godoc
// @Summary Авторизация пользователя
// @Description Авторизация пользователя по логину/паролю
// @Tags Account
// @Accept json
// @Produce json
// @Param login body model.LoginRequest true "Login"
// @Success 200 {object} model.Profile
// @Router /account/login [post]
func (h *AccountHandler) Login(w http.ResponseWriter, r *http.Request) {
	request := &model.LoginRequest{}

	if err := json.NewDecoder(r.Body).Decode(request); err != nil {
		h.logger.Error(err.Error())
		util.BadRequest(w, ErrFailedToLogin.Error())
		return
	}

	// TODO: валидация формы
	if len(request.Email) == 0 || len(request.Password) == 0 {
		util.BadRequest(w, ErrInvalidForm.Error())
		return
	}

	user, err := h.repo.FindByEmail(request.Email)
	if err != nil {
		h.logger.Error(err.Error())
		util.BadRequest(w, ErrUserNotFound.Error())
		return
	}

	if util.ComparePasswords(user.PasswordHash, request.Password) {
		response := model.AuthResponse{
			Token: util.CreateToken(h.config, user.Id),
			Profile: model.Profile{
				Email:            user.Email,
				Name:             user.Name,
				Username:         user.Username,
				RegistrationDate: user.RegistrationDate,
			},
		}
		util.JSONResponse(w, response)
	} else {
		util.BadRequest(w, ErrIncorrectPassword.Error())
	}
}

// Registration godoc
// @Summary Регистрация пользователя
// @Description Регистрация пользователя по почте/паролю
// @Tags Account
// @Accept json
// @Produce json
// @Param registration body model.RegistrationRequest true "Registration"
// @Success 200 {object} model.Profile
// @Router /account/registration [post]
func (h *AccountHandler) Registration(w http.ResponseWriter, r *http.Request) {
	request := &model.RegistrationRequest{}

	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		h.logger.Error(err.Error())
		util.BadRequest(w, ErrFailedToRegister.Error())
		return
	}

	// TODO: валидация формы
	if len(request.Email) == 0 || len(request.Password) == 0 {
		util.BadRequest(w, ErrInvalidForm.Error())
		return
	}

	user, _ := h.repo.FindByEmail(request.Email)
	if user != nil {
		util.BadRequest(w, ErrUserAlreadyExists.Error())
		return
	}

	hash, err := util.GetHash(request.Password)
	if err != nil {
		h.logger.Error(err.Error())
		util.BadRequest(w, ErrFailedToRegister.Error())
		return
	}

	lastId, err := h.repo.LastId()
	if err != nil {
		h.logger.Error(err.Error())
		util.BadRequest(w, ErrFailedToRegister.Error())
		return
	}

	username := "user" + strconv.FormatInt(lastId+1, 10)

	newUser := &model.User{
		Email:            request.Email,
		PasswordHash:     hash,
		RegistrationDate: time.Now(),
		Name:             username,
		Username:         username,
		Phone:            sql.NullString{},
		EmailConfirmed:   false,
		PhoneConfirmed:   false,
	}

	err = h.repo.Create(newUser)
	if err != nil {
		h.logger.Error(err.Error())
		util.BadRequest(w, ErrFailedToRegister.Error())
	} else {
		response := model.AuthResponse{
			Token: util.CreateToken(h.config, newUser.Id),
			Profile: model.Profile{
				Email:            newUser.Email,
				Name:             newUser.Name,
				Username:         newUser.Username,
				RegistrationDate: newUser.RegistrationDate,
			},
		}
		util.JSONResponse(w, response)
	}
}

// Profile godoc
// @Summary Информация по профилю
// @Description Получить информацию по профилю
// @Tags Account
// @Accept json
// @Produce json
// @Param Authentication header string true "Authentication" default(Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySWQiOjIsImF1ZCI6IndlYi1hcHAiLCJleHAiOjE2NDQwMjA2ODAsImlzcyI6IndlYi1hcGkifQ.OhG88pH6ArKjKTrQ0aNL3Ffy3Z3a5-fvXqhzuZCrqFo)
// @Success 200 {object} model.Profile
// @Router /account/profile [get]
func (h *AccountHandler) Profile(w http.ResponseWriter, r *http.Request) {
	userId, ok := r.Context().Value("UserId").(int64)
	if !ok {
		h.logger.Error(ErrFailedToProfile.Error())
		util.BadRequest(w, ErrFailedToProfile.Error())
		return
	}

	user, err := h.repo.GetById(userId)
	if err != nil {
		h.logger.Error(err.Error())
		util.BadRequest(w, ErrFailedToProfile.Error())
		return
	}

	response := model.Profile{
		Email:            user.Email,
		Name:             user.Name,
		Username:         user.Username,
		RegistrationDate: user.RegistrationDate,
	}

	util.JSONResponse(w, response)
}
