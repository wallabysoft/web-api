package util

import (
	"bytes"
	"fmt"

	"github.com/sirupsen/logrus"
)

// MyLogFormatter for formated logs
type MyLogFormatter struct {
}

// Format function for interface
func (f *MyLogFormatter) Format(entry *logrus.Entry) ([]byte, error) {
	var out *bytes.Buffer

	if entry.Buffer != nil {
		out = entry.Buffer
	} else {
		out = &bytes.Buffer{}
	}

	out.Write([]byte(fmt.Sprintf("%s %s %s",
		entry.Time.Format("2006-01-02T15:04:05"),
		entry.Level.String(),
		entry.Message)))

	var keys []string = make([]string, 0, len(entry.Data))
	for k := range entry.Data {
		keys = append(keys, k)
	}

	if len(keys) > 0 {
		out.WriteByte(' ')

		lastKeyIdx := len(keys) - 1

		for i, key := range keys {
			f.appendKeyValue(out, key, entry.Data[key], lastKeyIdx != i)
		}
	}

	out.WriteByte('\n')

	return out.Bytes(), nil
}

func (f *MyLogFormatter) appendKeyValue(b *bytes.Buffer, key string, value interface{}, appendSpace bool) {
	b.WriteString(key)
	b.WriteByte('=')
	f.appendValue(b, value)

	if appendSpace {
		b.WriteByte(' ')
	}
}

func (f *MyLogFormatter) appendValue(b *bytes.Buffer, value interface{}) {
	switch value := value.(type) {
	case string:
		b.WriteString(value)
	case error:
		errmsg := value.Error()
		b.WriteString(errmsg)
	default:
		fmt.Fprint(b, value)
	}
}
