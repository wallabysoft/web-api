package util

import (
	"time"

	"web-api/internal/app/model"

	"github.com/dgrijalva/jwt-go"
)

func CreateToken(config *model.Config, userId int64) string {
	claims := model.Claims{
		UserId: userId, // Идентификатор пользователя
		StandardClaims: jwt.StandardClaims{
			Audience:  config.JWT.Audience,                                                    // Потребитель
			ExpiresAt: time.Now().Add(time.Duration(config.JWT.Expires) * time.Minute).Unix(), // Время жизни в минутах
			Issuer:    config.JWT.Issuer,                                                      // Издатель
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	result, _ := token.SignedString([]byte(config.JWT.Key))

	return result
}

// TODO: Функция для парсинга токена
// func ParseToken(config *model.Config) (*jwt.Token, error) {
// 	token, err := jwt.ParseWithClaims(tokenParts[1], &claims, func(token *jwt.Token) (interface{}, error) {
// 		return []byte(config.JWT.Key), nil
// 	})

// 	return token, err
// }
