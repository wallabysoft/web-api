#!/bin/bash

sudo systemctl stop stories-api
cd ~/go/src/web-api
git pull
go get ./...
go install ./cmd/web-api
sudo systemctl start stories-api